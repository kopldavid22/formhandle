import { useState } from "react";
const useInputs = (validateValue) => {
  const [enteredValue, setEnteredValue] = useState("");
  const [isTouched, setIsTouched] = useState(false);

  const valueIsValid = validateValue(enteredValue);
  const hasError = !valueIsValid && isTouched;

  const handleValue = (event) => {
    setEnteredValue(event.target.value);
  };
  const valueBlueHandler = () => {
    setIsTouched(true);
  };

  const resetForm = () => {
    setEnteredValue("");
    setIsTouched(false);
  };
  return {
    value: enteredValue,
    isValid: valueIsValid,
    hasError,
    handleValue,
    valueBlueHandler,
    resetForm,
  };
};
export default useInputs;
