import SimpleInput from "./components/SimpleInput";
import BasicForm from "./components/BasicForm";

function App() {
  return (
    <div className="app">
      <h3>Hook vs.</h3>
      <SimpleInput />
      <h3>Very long syntax</h3>
      <BasicForm />
    </div>
  );
}

export default App;
