import useInputs from "../hooks/use-inputs";
const BasicForm = () => {
  const {
    value: enteredName,
    isValid: nameIsValid,
    hasError: nameError,
    handleValue: handleName,
    valueBlueHandler: nameBlurHandler,
    resetForm: resetNameInput,
  } = useInputs((value) => value.trim() !== "");

  const {
    value: enteredLastName,
    isValid: lastNameIsValid,
    hasError: lastNameError,
    handleValue: handleLastName,
    inputBlurHandler: lastNameBlurHandler,
    resetForm: resetLastNameInput,
  } = useInputs((value) => value.trim() !== "");

  const {
    value: enteredEmail,
    isValid: emailIsValid,
    hasError: emailError,
    handleValue: handleEmail,
    inputBlurHandler: emailBlurHandler,
    resetForm: resetEmailInput,
  } = useInputs((value) => value.trim() !== "" && value.trim().includes("@"));

  let isValid = false;
  if (nameIsValid && lastNameIsValid && emailIsValid) {
    isValid = true;
  }

  const formSubmissionHandler = (event) => {
    event.preventDefault();
    if (!nameIsValid) {
      return;
    }
    if (!lastNameIsValid) {
      return;
    }
    if (!emailIsValid) {
      return;
    }

    //restart forms
    resetNameInput();
    resetLastNameInput();
    resetEmailInput();
  };
  const nameInputClasses = nameIsValid
    ? "form-control invalid"
    : "form-control";
  const lastNameInputClasses = nameIsValid
    ? "form-control invalid"
    : "form-control";
  const emailInputClasses = emailIsValid
    ? "form-control invalid"
    : "form-control";
  return (
    <form onSubmit={formSubmissionHandler}>
      <div className={nameInputClasses}>
        <label htmlFor="name">First Name</label>
        <input
          onChange={handleName}
          value={enteredName}
          onBlur={nameBlurHandler}
          type="text"
          id="name1"
        />
        {nameError && <p className="error-text">Name can not be blank</p>}
      </div>
      <div className={lastNameInputClasses}>
        <label htmlFor="lastName">Last Name</label>
        <input
          onChange={handleLastName}
          value={enteredLastName}
          onBlur={lastNameBlurHandler}
          type="text"
          id="lastname1"
        />

        {lastNameError && <p className="error-text">Name can not be blank</p>}
      </div>

      <div className={emailInputClasses}>
        <label htmlFor="email">E-Mail Address</label>
        <input
          onChange={handleEmail}
          value={enteredEmail}
          onBlur={emailBlurHandler}
          type="text"
          id="email1"
        />

        {emailError && <p className="error-text">Please fill a valid email</p>}
      </div>
      <div className="form-actions">
        <button disabled={!isValid}>Submit</button>
      </div>
    </form>
  );
};

export default BasicForm;
